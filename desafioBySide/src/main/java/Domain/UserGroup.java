package Domain;

import java.util.ArrayList;

public class UserGroup {
    private ArrayList<User> users;

    public UserGroup() {
        this.users = new ArrayList<>();
    }

    public void addUserToGroup(User user){
        users.add(user);
    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "users=" + users +
                '}';
    }
}
