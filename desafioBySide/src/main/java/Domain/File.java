package Domain;

import java.time.LocalDate;

public class File extends Input {
    private String content;
    private int size;


    public File(String name, Directory parent, User owner, String perms) {
        super(name,parent, owner, perms);
        this.size=0;
        content="";
    }

    @Override
    public int size() {
        return size;
    }

    public void changeSize(int size){
        this.size=size;
    }

    public String getContent()
    { 
        return content; 
    } 
    public void setContent(String c)
    {
        content = c;
        changeSize(c.length());
    }

    public File clone(){
        File file = new File(this.getName(), this.getParentDirectory(), this.getOwner(), this.getPermissions());
        file.changeSize(this.size);
        file.setContent(this.content);
        return file;
    }

    @Override
    public String toString() {
        String string = "";
        LocalDate creation = getcreationDate();
        LocalDate access = getLastAccessDate();
        LocalDate update = getLastUpdateDate();
        string += getPermissions()+" "+getOwner().toString()+" "+size+" "+creation+" "+access+" "+update+" "+getName();
        return string;
    }
}
