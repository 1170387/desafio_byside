package Domain;

import java.util.ArrayList;

public class FileSystem {
    private ArrayList<User> users;
    private Directory systemFiles;

    /*
    Creates an new system with the user admin and with an directory root
     */
    public FileSystem(){
        this.users= new ArrayList<>();
        this.users.add(new User("admin", "adminPassword123", new UserGroup()));
        users.get(0).getGroup().addUserToGroup(users.get(0));
        this.systemFiles=new Directory("root", null, users.get(0), "drwxrwxrwx");
    }

    public Directory getSystemFiles(){
        return systemFiles;
    }

    public User getUser(String userName){
        User user=null;
        for (User us: users) {
            if(us.compareUserName(userName))
                user=us;
        }
        return user;
    }

    public void addUser(User user){
        users.add(user);
    }
}
