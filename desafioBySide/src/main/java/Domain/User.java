package Domain;

import javax.swing.*;

public class User {
    private String name;
    private String password;
    private UserGroup group;

    public User(String name, String password, UserGroup userGroup){
        this.name=name;
        this.password=password;
        this.group=userGroup;
    }

    public boolean comparePassword(String passwordToCompare){
        return passwordToCompare.equals(password);
    }

    public boolean compareUserName(String userName){
        return userName.equals(name);
    }

    public String getName(){
        return name;
    }

    public UserGroup getGroup(){
        return group;
    }

    @Override
    public String
    toString() {
        return name;
    }
}
