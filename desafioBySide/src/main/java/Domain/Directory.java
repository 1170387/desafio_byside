package Domain;

import java.time.LocalDate;
import java.util.ArrayList;

public class Directory extends Input {
    private ArrayList<Input> contents;

    public Directory(String name, Directory parent, User user, String perms)
    {
        super(name, parent, user, perms);
        contents = new ArrayList<Input>();
    }
    public int size() 
    { 
        int size = 0; 
        for (Input input : contents) 
            size += input.size(); 
          
        return size; 
    } 
    public int numberOfFiles() 
    { 
        int count = 0; 
        for (Input input : contents) 
        { 
            if (input instanceof Directory) 
            { 
                count++; // Directory counts as a file 
                Directory d = (Directory) input; 
                count += d. numberOfFiles (); 
            } 
            else if (input instanceof File)             
                count++;             
        } 
        return count; 
    } 
  
    public boolean deleteInput(Input input) 
    { 
        return contents.remove(input); 
    } 
  
    public void addInput(Input input)
    {
        boolean have=false;
        for (Input in: getContents()) {
            if(in.getName()==input.getName()){
                have =true;
                break;
            }
        }
        if(have)
            input.changeName(input.getName()+ "_copy");
        contents.add(input);
    }  
  
    public ArrayList<Input> getContents()
    { 
        return contents; 
    }

    public void showContent(){
        for (Input input: contents) {
            if(input instanceof Directory)
                System.out.println(input.toString());
            else if(input instanceof File)
                System.out.println(input.toString());
        }
    }

    public String path(){
        String path = "";
        if(this.getName()=="root")
            path+="/";
        else {
            if(getParentDirectory().getName()=="root"){
                path += "/" + this.getName();
            }else{
                path += getParentDirectory().path() + "/" + this.getName();
            }
        }
        return path;
    }

    private void changeContent(ArrayList<Input> content){
        this.contents=content;
    }

    public Directory clone(){
        Directory directory = new Directory(this.getName(), this.getParentDirectory(), this.getOwner(), this.getPermissions());
        directory.changeContent(this.contents);
        return directory;
    }

    @Override
    public String toString() {
        String string = "";
        LocalDate creation = getcreationDate();
        LocalDate access = getLastAccessDate();
        LocalDate update = getLastUpdateDate();
        string += getPermissions()+" "+getOwner().toString()+" "+size()+" "+creation+" "+access+" "+update+" "+getName();
        return string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Directory))
            return false;
        Directory directory = (Directory) o;
        if(directory.getName().equals(this.getName()))
            return true;
        return contents.equals(directory.contents);
    }
}
