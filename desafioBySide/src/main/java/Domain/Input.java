package Domain;

import java.time.LocalDate;

public abstract class Input {
    private Directory parentDirectory;
    private String name;
    private LocalDate creationDate;
    private LocalDate lastAccessDate;
    private LocalDate lastUpdateDate;
    private String permissions;
    private User owner;
    
    public Input(String name, Directory parent, User owner, String permissions) {
        this.name=name;
        this.parentDirectory=parent;
        this.owner=owner;
        this.creationDate= LocalDate.now();
        this.lastAccessDate= LocalDate.now();
        this.lastUpdateDate= LocalDate.now();
        this.permissions=permissions;
    }
    public abstract int size();

    public String getPermissions(){
        return permissions;
    }

    public User getOwner(){
        return owner;
    }

    public LocalDate getcreationDate()
    { 
        return creationDate; 
    } 
    
    public LocalDate getLastUpdateDate()
    { 
        return lastUpdateDate; 
    }
    
    public LocalDate getLastAccessDate()
    { 
        return lastAccessDate; 
    }
    
    public String getName() 
    { 
        return name; 
    }

    public Directory getParentDirectory() {
        return parentDirectory;
    }

    public void changeName(String name) 
    { 
        this.name = name; 
    }

    public void changeParent(Directory parent){
        this.parentDirectory=parent;
    }

    public void changeLastAccessDate(){
        this.lastAccessDate=LocalDate.now();
    }

    public void changeLastUpdateDate(){
        this.lastUpdateDate=LocalDate.now();
        this.lastAccessDate=LocalDate.now();
    }

    public boolean delete(){
        if (parentDirectory == null)
            return false;
        return parentDirectory.deleteInput(this);
    }
}
