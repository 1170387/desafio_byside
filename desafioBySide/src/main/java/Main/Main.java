package Main;

import Bootstrap.Bootstrap;
import Domain.FileSystem;
import UI.AuthUI;
import UI.BaseUI;

public class Main
{

    public static void main(String[] args)
    {
        FileSystem fileSystem =Bootstrap.boot();
        AuthUI.show(fileSystem);
    }

}