package UI;

import Bootstrap.Bootstrap;
import Domain.Directory;
import Domain.File;
import Domain.Input;
import Domain.User;

import java.util.Scanner;

public class BaseUI{
    private static Directory root;
    private static Directory actualDirectory;
    private static User user;

    public BaseUI(User user, Directory root){
        this.user=user;
        this.root=root;
    }
    public static void show(Directory directory){
        directory.changeLastAccessDate();
        actualDirectory=directory;
        System.out.println(actualDirectory.path()+" -> ");
        Scanner scan = new Scanner(System.in);
        String command = scan.nextLine();
        commandAnalyser(command);
    }

    private static void commandAnalyser(String command){
        String[] splitted = command.split(" ");
        switch (splitted[0]) {
            case "ls":
                lsCommand(command);
                break;
            case "cd":
                cdCommand(command);
                break;
            case "cat":
                catCommand(command);
                break;
            case "nano":
                nanoCommand(command);
                break;
            case "mv":
                mvCommand(command);
                break;
            case "mkdir":
                mkdirCommand(command);
                break;
            case "rm":
                rmCommand(command);
                break;
            case "cp":
                cpCommand(command);
                break;
            case "":
                show(actualDirectory);
                break;
            default:
                System.out.println(command+": command not found");
                show(actualDirectory);
                break;
        }
    }

    private static void lsCommand(String command){
        String[] splitted = command.split(" ");
        Directory directory = actualDirectory;
        File file = null;
        if(splitted.length==2){
            for (Input input: actualDirectory.getContents()) {
                if(input instanceof Directory){
                    if(input.getName().equals(splitted[1])){
                        directory=(Directory)input;
                    }
                }else if(input instanceof File){
                    if(input.getName().equals(splitted[1])){
                        file=(File)input;
                    }
                }
            }
            if(directory.equals(actualDirectory)&&file==null){
                System.out.println("ls: cannot access '"+splitted[1]+"': No such File or Directory");
                show(actualDirectory);
            }else if(file!=null){
                System.out.println(file.toString());
                show(actualDirectory);
            }else{
                if(havePermission(user,directory, 1)){
                    directory.showContent();
                    directory.changeLastAccessDate();
                    show(directory);
                }else{
                    System.out.println("ls: cannot open directory '"+splitted[1]+"': permission denied");
                    show(actualDirectory);
                }
            }
        }else if(splitted.length==1){
            directory.showContent();
            show(directory);
        }else{
            System.out.println("ls: incorrect command");
            show(actualDirectory);
        }

    }

    private static void cdCommand(String command) {
        String[] splitted = command.split(" ");
        Directory directory = actualDirectory;
        boolean file=false;
        if(splitted.length==2) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = (Directory) input;
                    }
                } else if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = true;
                    }
                }
            }
            if(splitted[1].equals("..")){
                if(actualDirectory.getParentDirectory()==null){
                    show(actualDirectory);
                }else {
                    show(actualDirectory.getParentDirectory());
                }

            }else if (directory.equals(actualDirectory) && !file) {
                System.out.println("cd: " + splitted[1] + ": No such File or Directory");
                show(actualDirectory);
            } else if (file) {
                System.out.println("cd: " + splitted[1] + ": Not a Directory");
                show(actualDirectory);
            } else {
                if (havePermission(user, directory, 1)) {
                    show(directory);
                } else {
                    System.out.println("cd: " + splitted[1] + ": permission denied");
                    show(actualDirectory);
                }
            }
        }else{
            System.out.println("cd: incorrect command");
            show(actualDirectory);
        }
    }

    private static void catCommand(String command) {
        String[] splitted = command.split(" ");
        File file=null;
        boolean directory = false;
        if(splitted.length==2) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = (File) input;
                    }
                } else if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = true;
                    }
                }
            }
            if (file == null && !directory) {
                System.out.println("cat: " + splitted[1] + ": No such File or Directory");
                show(actualDirectory);
            } else if (directory) {
                System.out.println("cat: " + splitted[1] + ": Is a Directory");
                show(actualDirectory);
            } else {
                if (havePermission(user, file, 1)) {
                    System.out.println(file.getContent());
                    file.changeLastAccessDate();
                    show(actualDirectory);
                } else {
                    System.out.println("cat: " + splitted[1] + ": permission denied");
                    show(actualDirectory);
                }
            }
        }else{
            System.out.println("cat: incorrect command");
            show(actualDirectory);
        }
    }

    private static void nanoCommand(String command) {
        String[] splitted = command.split(" ");
        File file=null;
        boolean directory = false;
        String[] splittedForMessage = command.split(">");
        if(splitted.length>2 && splittedForMessage.length==2) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = (File) input;
                    }
                } else if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = true;
                    }
                }
            }
            if (file == null && !directory) {
                File f=Bootstrap.createFile(splitted[1], actualDirectory,user);
                f.setContent(splittedForMessage[1].trim());
                show(actualDirectory);
            } else if (directory) {
                System.out.println("nano: " + splitted[1] + ": Is a Directory");
                show(actualDirectory);
            } else {
                if (havePermission(user, file, 2)) {
                    file.setContent(splittedForMessage[1].trim());
                    file.getLastUpdateDate();
                    System.out.println(file.getContent());
                    show(actualDirectory);
                } else {
                    System.out.println("nano: " + splitted[1] + ": permission denied");
                    show(actualDirectory);
                }
            }
        }else{
            System.out.println("nano: incorrect command");
            show(actualDirectory);
        }
    }

    private static void mvCommand(String command) {
        String[] splitted = command.split(" ");
        Directory directory = actualDirectory;
        File file=null;
        if(splitted.length==3) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = (Directory) input;
                    }
                } else if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = (File)input;
                    }
                }
            }
            if(directory.equals(actualDirectory)&& file==null){
                System.out.println("mv: " + splitted[1] + ": No such File or Directory");
                show(actualDirectory);
            }else {
                Input inputToMove;
                if(file==null){
                    inputToMove=directory;
                }else{
                    inputToMove=file;
                }
                if(splitted[2].split("/").length>0){
                    String[] path=splitted[2].split("/");
                    Directory target = isAnValidPath(path);
                    if(target!=null) {
                        if (havePermission(user, inputToMove, 1) && havePermission(user, target, 2)) {
                            actualDirectory.deleteInput(inputToMove);
                            actualDirectory.changeLastUpdateDate();
                            inputToMove.changeParent(target);
                            show(actualDirectory);
                        } else {
                            System.out.println("mv: " + splitted[1] + " or " + splitted[2] + ": permission denied");
                            show(actualDirectory);
                        }
                    }else{
                        System.out.println("mv: " + splitted[2] + ": Is not a valid directory path");
                        show(actualDirectory);
                    }
                }else{
                    if (havePermission(user, inputToMove, 1) && havePermission(user, inputToMove, 2)) {
                        inputToMove.changeName(splitted[2]);
                        actualDirectory.changeLastUpdateDate();
                        show(actualDirectory);
                    } else {
                        System.out.println("mv: " + splitted[1] + ": permission denied");
                        show(actualDirectory);
                    }
                }
            }
        }else{
            System.out.println("mv: incorrect command");
            show(actualDirectory);
        }
    }

    private static void mkdirCommand(String command) {
        String[] splitted = command.split(" ");
        if(splitted.length==2) {
            Bootstrap.createDirectory(splitted[1], actualDirectory, user);
            show(actualDirectory);
        }else{
            System.out.println("mkdir: missing directory name");
        }
    }

    private static void rmCommand(String command) {
        String[] splitted = command.split(" ");
        Directory directory = actualDirectory;
        File file=null;
        if(splitted.length==2) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = (Directory) input;
                    }
                } else if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = (File)input;
                    }
                }
            }
            if(directory.equals(actualDirectory)&& file==null){
                System.out.println("rm: cannot remove " + splitted[1] + ": No such File or Directory");
                show(actualDirectory);
            }else {
                Input inputToMove;
                if(file==null){
                    inputToMove=directory;
                }else{
                    inputToMove=file;
                }

                if (havePermission(user, inputToMove, 2)) {
                    actualDirectory.deleteInput(inputToMove);
                    actualDirectory.changeLastUpdateDate();
                    inputToMove.changeParent(null);
                    show(actualDirectory);
                } else {
                    System.out.println("rm: cannot remove " + splitted[1] + ": permission denied");
                    show(actualDirectory);
                }

            }
        }else{
            System.out.println("rm: incorrect command");
            show(actualDirectory);
        }
    }

    private static void cpCommand(String command) {
        String[] splitted = command.split(" ");
        Directory directory = actualDirectory;
        File file=null;
        if(splitted.length==3) {
            for (Input input : actualDirectory.getContents()) {
                if (input instanceof Directory) {
                    if (input.getName().equals(splitted[1])) {
                        directory = (Directory) input;
                    }
                } else if (input instanceof File) {
                    if (input.getName().equals(splitted[1])) {
                        file = (File)input;
                    }
                }
            }
            if(directory.equals(actualDirectory)&& file==null){
                System.out.println("cp: " + splitted[1] + ": No such File or Directory");
                show(actualDirectory);
            }else {
                Input inputToCopy;
                if(file==null){
                    inputToCopy=directory;
                }else{
                    inputToCopy=file;
                }
                if(splitted[2].split("/").length>0 || splitted[2].equals("/")){
                    Directory target;
                    if(splitted[2].equals("/")){
                        target = root;
                    }else {
                        String[] path = splitted[2].split("/");
                        target = isAnValidPath(path);
                    }
                    if(target!=null) {
                        if (havePermission(user, inputToCopy, 1) && havePermission(user, target, 2)) {
                            if(inputToCopy instanceof File) {
                                File f = ((File) inputToCopy).clone();
                                f.changeParent(target);
                                target.addInput(f);
                            }else {
                                Directory d = ((Directory) inputToCopy).clone();
                                d.changeParent(target);
                                target.addInput(d);
                            }
                            show(actualDirectory);
                        } else {
                            System.out.println("cp: " + splitted[1] + " or " + splitted[2] + ": permission denied");
                            show(actualDirectory);
                        }
                    }else{
                        System.out.println("cp: " + splitted[2] + ": Is not a valid directory path");
                        show(actualDirectory);
                    }
                }else{
                    System.out.println("cp: " + splitted[2] + ": is not a path");
                    show(actualDirectory);
                }
            }
        }else{
            System.out.println("cp: incorrect command");
            show(actualDirectory);
        }
    }

    private static boolean havePermission(User user, Input input, int type){
        String[] perms = {"r","w","x"};
        String[] permissions = input.getPermissions().split("");
        if(user.compareUserName(input.getOwner().getName())) {
            if(permissions[type].equals(perms[type-1])) {
                return true;
            }else{
                return false;
            }
        }else if(user.getGroup().equals(input.getOwner().getGroup())){
            if(permissions[type+3].equals(perms[type-1])) {
                return true;
            }else{
                return false;
            }
        }else{
            if(permissions[type+6].equals(perms[type-1])) {
                return true;
            }else{
                return false;
            }
        }
    }

    private static Directory isAnValidPath(String[] path){
        Directory actual = root;
        boolean have;
        for(int i=0; i<path.length; i++){
            have=false;
            for (Input input: actual.getContents()) {
                if(input instanceof Directory) {
                    if (path[i].equals(input.getName())) {
                        actual = (Directory) input;
                        have=true;
                        break;
                    }
                }
            }
            if(!have){
                return null;
            }
        }
        return actual;
    }

}
