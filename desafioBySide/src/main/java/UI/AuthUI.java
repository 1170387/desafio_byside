package UI;

import Domain.FileSystem;
import Domain.User;

import java.util.Scanner;

public class AuthUI {
    public static void show(FileSystem fileSystem){
        System.out.println("Name: ");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();
        User user = fileSystem.getUser(name);
        System.out.println("Password: ");
        String password = scan.nextLine();
        if(user!=null){
            if(user.comparePassword(password)) {
                BaseUI baseUI = new BaseUI(user, fileSystem.getSystemFiles());
                baseUI.show(fileSystem.getSystemFiles());
            }else{
                System.out.println("Incorrect Username or Password!");
                show(fileSystem);
            }
        }else{
            System.out.println("Incorrect Username or Password!");
            show(fileSystem);
        }
    }
}
