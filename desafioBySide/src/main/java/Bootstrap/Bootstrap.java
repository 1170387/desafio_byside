package Bootstrap;

import Domain.*;

public class Bootstrap {

    public static FileSystem boot() {
        User user1 = new User("user1", "userPassword123", new UserGroup());
        FileSystem fileSystem = new FileSystem();
        fileSystem.addUser(user1);
        Directory root = fileSystem.getSystemFiles();
        Directory Directory1 = createDirectory("pasta1", root, user1);
        Directory Directory2 = createDirectory("pasta2", root, user1);
        Directory Directory3 = createDirectory("pasta3", Directory1, user1);
        Directory Directory4 = createDirectory("pasta4", Directory1, user1);
        File fileDirectory1 = createFile("File1", Directory1, user1);
        File fileRoot = createFile("File_in_root", root, fileSystem.getUser("admin"));

        return fileSystem;
    }

    public static Directory createDirectory(String name, Directory parent, User user){
        if(parent!=null){
            Directory directory;
            if(user.compareUserName("admin"))
                directory = new Directory(name, parent, user, "drwxrwx---");
            else
                directory = new Directory(name, parent, user, "drwxrwxrwx");
            parent.addInput(directory);
            return directory;
        }else{
            return null;
        }
    }

    public static File createFile(String name, Directory parent, User user){
        if(parent!=null){
            for (Input input: parent.getContents()) {
                if(input.getName()==name){
                    name += " - copy";
                }
            }
            File file;
            if(user.compareUserName("admin"))
                file = new File(name, parent, user, "drwxrwx---");
            else
                file = new File(name, parent, user, "drwxrwxrwx");
            parent.addInput(file);
            return file;
        }else{
            return null;
        }
    }
}