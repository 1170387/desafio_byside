# BySide Challenge

This project was designed to solve a challenge launched by bySide, which was to develop a file system.

In this file system there are **only two users**, the admin and the user1, because it is an illustrative project.
**Both of these users have to make login into the system.**

|Users|Name |Password        |
|:---:|:---:|:--------------:|
|Admin|admin|adminPassword123|
|User |user1|userPassword123 |

## Functionalities
### ls [directory] 

directory **[optional]** - shows the information of all files/directories in the directory where the user is.

* _Output_ **[permissions][userName][size][creationDate][lastAccessDate][lastUpdateDate][file/directory name]**
* _Example_ **dwxrwxrwx user1 0 2020-01-09 2020-01-09 2020-01-09 pasta1**

### cd [directory] 

directory **[required]** - moves to the specified directory

### cat [file] 

file **[required]** - shows the content of the specified file

* _Output_ - file content

### nano [file] > [msg] 

file **[required]**, msg **[required]** - changes the content of file. If file dosen't exists it will be created in the directory where the user is

* _Output_ - file changed content

### mv [file] [directory] 

file **[required]**, directory **[required]** - moves file, that can be an file or an directory, to the specified directory path.
This command **also can rename** the file/directory in [file], by writing the new name in [directory]

### mkdir [directory]

directory **[required]** - creates an directory with name [directory], in the directory where the user is

### rm [file]

file **[required]** - removes the file/directory specified in [file].

### cp [file] [destination]

file **[required]**, destination **[required]** - copy the file/directory specified in [file] to the destination path specified in [destination]